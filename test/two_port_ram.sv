module two_port_ram #(
    parameter   WIDTH   = 16,
    parameter   DEPTH   = 256,
    parameter   EDAC    = 0
) (
    input                               write_clock,
    input                               write_reset_n,
    input                               write_enable,
    input           [$clog2(DEPTH)-1:0] write_address,
    input           [WIDTH-1:0]         write_data,
    input                               read_clock,
    input                               read_reset_n,
    input                               read_enable,
    input           [$clog2(DEPTH)-1:0] read_address,

    output  logic   [WIDTH-1:0]         read_data,
    output                              read_correctable,
    output                              read_error
);


generate
    if (EDAC) begin

    end
    else begin
        // if (WIDTH == 16 && DEPTH == 256)    two_port_ram_16x256(.*);
        // else begin
            logic [DEPTH-1:0][WIDTH-1:0] memory; /* synthesis syn_ramstyle = "registers" */;

            assign read_correctable = 0;
            assign read_error       = 0;

            always_ff @(posedge write_clock) begin
                if (write_enable) begin
                    memory[write_address] <= write_data;
                end
            end

            always_ff @(posedge read_clock or negedge read_reset_n) begin
                if (!read_reset_n) begin
                    read_data <= 0;
                end
                else begin
                    read_data <= memory[read_address];
                end
            end
        // end
    end
endgenerate


endmodule

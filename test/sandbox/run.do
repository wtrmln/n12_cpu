quit -sim
.main clear

file delete -force presynth
vlib presynth
vmap presynth presynth
vmap proasic3e "C:/Microsemi/Libero_SoC_v11.9/Designer/lib/modelsim/precompiled/vlog/proasic3e"

vlog -sv -work presynth \
    ../../stack.sv \
    ../../n12_cpu.sv \
    ../two_port_ram.sv \
    program_code.svh \
    testbench.sv

vsim -L proasic3e -L presynth presynth.testbench
add log -r /*

if [file exists "wave.do"] {
    do "wave.do"
}

run -all

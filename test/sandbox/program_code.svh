package program_code_package;


parameter logic ENABLE_INTERRUPT            = 1'b0;
parameter logic ENABLE_ADD                  = 1'b0;
parameter logic ENABLE_LESS_THAN_SIGNED     = 1'b0;
parameter logic ENABLE_LESS_THAN_UNSIGNED   = 1'b0;
parameter logic ENABLE_RIGHT_SHIFT          = 1'b0;
parameter logic ENABLE_MULTIPLY             = 1'b0;


parameter logic [11:0] MAIN_ADDRESS = 12'd16;
parameter logic [11:0] ISR_ADDRESS  = 12'd16;


// Constants
parameter ADDRESS_NETWORK_ID      = 256;
parameter ADDRESS_NETWORK_ADDRESS = 257;
parameter ADDRESS_NETWORK_DATA    = 258;
parameter ADDRESS_STOP            = 259;


parameter logic [0:53][11:0] PROGRAM_CODE = {
    /*   0 */   12'b110110000001,    // ..\..\base.fth: 64: : rot >r
    /*   1 */   12'b111100000001,    // ..\..\base.fth: 64: swap
    /*   2 */   12'b111001000010,    // ..\..\base.fth: 64: r>
    /*   3 */   12'b111101100001,    // ..\..\base.fth: 64: swap ;
    /*   4 */   12'b001000000001,    // testbench.fth:  9: : memory_map_write ADDRESS_NETWORK_DATA
    /*   5 */   12'b000100000010,    // testbench.fth:  9: : memory_map_write ADDRESS_NETWORK_DATA
    /*   6 */   12'b110100010001,    // (testbench.fth:9: !) ..\..\base.fth: 59: N->[T]
    /*   7 */   12'b110100000001,    // (testbench.fth:9: !) ..\..\base.fth: 59: drop
    /*   8 */   12'b001000000001,    // testbench.fth: 10: ADDRESS_NETWORK_ADDRESS
    /*   9 */   12'b000100000001,    // testbench.fth: 10: ADDRESS_NETWORK_ADDRESS
    /*  10 */   12'b110100010001,    // (testbench.fth:10: !) ..\..\base.fth: 59: N->[T]
    /*  11 */   12'b110100000001,    // (testbench.fth:10: !) ..\..\base.fth: 59: drop
    /*  12 */   12'b001000000001,    // testbench.fth: 11: ADDRESS_NETWORK_ID
    /*  13 */   12'b000100000000,    // testbench.fth: 11: ADDRESS_NETWORK_ID
    /*  14 */   12'b110100010001,    // (testbench.fth:11: !) ..\..\base.fth: 59: N->[T]
    /*  15 */   12'b110101100001,    // (testbench.fth:11: !) ..\..\base.fth: 59: drop ;
    /*  16 */   12'b001000000101,    // testbench.fth: 17: : main 5
    /*  17 */   12'b001010000010,    // testbench.fth: 17: 33333
    /*  18 */   12'b000100110101,    // testbench.fth: 17: 33333
    /*  19 */   12'b001010011000,    // testbench.fth: 17: 0x9876
    /*  20 */   12'b000101110110,    // testbench.fth: 17: 0x9876
    /*  21 */   12'b011000000100,    // testbench.fth: 17: memory_map_write
    /*  22 */   12'b001000001010,    // testbench.fth: 20: 10
    /*  23 */   12'b110000000110,    // testbench.fth: 20: begin 1-
    /*  24 */   12'b111000000000,    // testbench.fth: 20: dup
    /*  25 */   12'b001000000000,    // (testbench.fth:20: 0=) ..\..\base.fth: 56: 0
    /*  26 */   12'b110100001011,    // (testbench.fth:20: 0=) ..\..\base.fth: 56: =
    /*  27 */   12'b101000010111,    // testbench.fth: 20: until
    /*  28 */   12'b001000000001,    // testbench.fth: 23: ADDRESS_NETWORK_ID
    /*  29 */   12'b000100000000,    // testbench.fth: 23: ADDRESS_NETWORK_ID
    /*  30 */   12'b110000000000,    // (testbench.fth:23: @) ..\..\base.fth: 60: noop
    /*  31 */   12'b110000000011,    // (testbench.fth:23: @) ..\..\base.fth: 60: [T]
    /*  32 */   12'b001000000001,    // testbench.fth: 24: ADDRESS_NETWORK_ADDRESS
    /*  33 */   12'b000100000001,    // testbench.fth: 24: ADDRESS_NETWORK_ADDRESS
    /*  34 */   12'b110000000000,    // (testbench.fth:24: @) ..\..\base.fth: 60: noop
    /*  35 */   12'b110000000011,    // (testbench.fth:24: @) ..\..\base.fth: 60: [T]
    /*  36 */   12'b001000000001,    // testbench.fth: 25: ADDRESS_NETWORK_DATA
    /*  37 */   12'b000100000010,    // testbench.fth: 25: ADDRESS_NETWORK_DATA
    /*  38 */   12'b110000000000,    // (testbench.fth:25: @) ..\..\base.fth: 60: noop
    /*  39 */   12'b110000000011,    // (testbench.fth:25: @) ..\..\base.fth: 60: [T]
    /*  40 */   12'b001000001010,    // testbench.fth: 27: 10
    /*  41 */   12'b110000000110,    // testbench.fth: 27: begin 1-
    /*  42 */   12'b111000000000,    // testbench.fth: 27: dup
    /*  43 */   12'b001000000000,    // (testbench.fth:27: 0=) ..\..\base.fth: 56: 0
    /*  44 */   12'b110100001011,    // (testbench.fth:27: 0=) ..\..\base.fth: 56: =
    /*  45 */   12'b101000101001,    // testbench.fth: 27: until
    /*  46 */   12'b001000000001,    // testbench.fth: 30: 1
    /*  47 */   12'b001000000001,    // testbench.fth: 30: ADDRESS_STOP
    /*  48 */   12'b000100000011,    // testbench.fth: 30: ADDRESS_STOP
    /*  49 */   12'b110100010001,    // (testbench.fth:30: !) ..\..\base.fth: 59: N->[T]
    /*  50 */   12'b110100000001,    // (testbench.fth:30: !) ..\..\base.fth: 59: drop
    /*  51 */   12'b110000000000,    // testbench.fth: 32: begin noop
    /*  52 */   12'b100000110011,    // testbench.fth: 32: again
    /*  53 */   12'b110001100000     // testbench.fth: 33: ;
};


endpackage

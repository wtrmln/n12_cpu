import sys


sys.path.append("../..")
from n12_cpu import compiler


test_compiler = compiler()
program = test_compiler.compile_file("testbench.fth")

with open("program_code.svh", "w") as hdl_file:
    hdl_file.write("package program_code_package;\n\n\n")
    hdl_file.write(program.render_verilog())
    hdl_file.write("\n\nendpackage\n")

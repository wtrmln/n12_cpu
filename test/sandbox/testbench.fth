256 constant    ADDRESS_NETWORK_ID
257 constant    ADDRESS_NETWORK_ADDRESS
258 constant    ADDRESS_NETWORK_DATA
259 constant    ADDRESS_STOP


( network_id address data -- )
: memory_map_write
    ADDRESS_NETWORK_DATA !
    ADDRESS_NETWORK_ADDRESS !
    ADDRESS_NETWORK_ID !
;


: main
    ( Write value 0x9876 to network ID 5, address 33333 )
    5 33333 0x9876 memory_map_write

    ( 10 noops )
    10 begin 1- dup 0= until

    ( Read onto the stack from memory-mapped registers )
    ADDRESS_NETWORK_ID @
    ADDRESS_NETWORK_ADDRESS @
    ADDRESS_NETWORK_DATA @

    10 begin 1- dup 0= until

    ( Tell the simulator we're done )
    1 ADDRESS_STOP !

    begin noop again
;

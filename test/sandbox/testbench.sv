`timescale 1ns/1ns
`default_nettype none


module testbench();


parameter   CLOCK_PERIOD    = 20.0ns;

parameter   DATA_WIDTH      = 16;
parameter   DATA_DEPTH      = 8;
parameter   RETURN_DEPTH    = 8;
parameter   MEMORY_DEPTH    = 256;


wire                        cpu_clock;
wire                        cpu_reset_n;
logic                       cpu_interrupt;
logic                       cpu_enable;
logic   [11:0]              cpu_program_code;
wire    [DATA_WIDTH-1:0]    cpu_memory_read_data;

wire    [8:0]               cpu_program_address;
wire    [DATA_WIDTH-1:0]    cpu_memory_address;
wire    [DATA_WIDTH-1:0]    cpu_memory_write_data;
wire                        cpu_memory_write_data_enable;

n12_cpu #(
    .DATA_WIDTH                 (DATA_WIDTH),
    .DATA_DEPTH                 (DATA_DEPTH),
    .RETURN_DEPTH               (RETURN_DEPTH),
    .MAIN_ADDRESS               (program_code_package::MAIN_ADDRESS),
    .ISR_ADDRESS                (program_code_package::ISR_ADDRESS),
    .ENABLE_INTERRUPT           (program_code_package::ENABLE_INTERRUPT),
    .ENABLE_ADD                 (program_code_package::ENABLE_ADD),
    .ENABLE_LESS_THAN_SIGNED    (program_code_package::ENABLE_LESS_THAN_SIGNED),
    .ENABLE_LESS_THAN_UNSIGNED  (program_code_package::ENABLE_LESS_THAN_UNSIGNED),
    .ENABLE_RIGHT_SHIFT         (program_code_package::ENABLE_RIGHT_SHIFT),
    .ENABLE_MULTIPLY            (program_code_package::ENABLE_MULTIPLY)
) cpu(
    .clock                      (cpu_clock),
    .reset_n                    (cpu_reset_n),
    .interrupt                  (cpu_interrupt),
    .enable                     (cpu_enable),
    .program_code               (cpu_program_code),
    .memory_read_data           (cpu_memory_read_data),

    .program_address            (cpu_program_address),
    .memory_address             (cpu_memory_address),
    .memory_write_data          (cpu_memory_write_data),
    .memory_write_data_enable   (cpu_memory_write_data_enable)
);


wire                                memory_write_clock;
wire                                memory_write_reset_n;
wire                                memory_write_enable;
wire    [$clog2(MEMORY_DEPTH)-1:0]  memory_write_address;
wire    [DATA_WIDTH-1:0]            memory_write_data;
wire                                memory_read_clock;
wire                                memory_read_reset_n;
wire                                memory_read_enable;
wire    [$clog2(MEMORY_DEPTH)-1:0]  memory_read_address;

wire    [DATA_WIDTH-1:0]            memory_read_data;
wire                                memory_read_correctable;
wire                                memory_read_error;

two_port_ram #(
    .WIDTH  (DATA_WIDTH),
    .DEPTH  (MEMORY_DEPTH),
    .EDAC   (0)
) memory(
    .write_clock        (memory_write_clock),
    .write_reset_n      (memory_write_reset_n),
    .write_enable       (memory_write_enable),
    .write_address      (memory_write_address),
    .write_data         (memory_write_data),
    .read_clock         (memory_read_clock),
    .read_reset_n       (memory_read_reset_n),
    .read_enable        (memory_read_enable),
    .read_address       (memory_read_address),

    .read_data          (memory_read_data),
    .read_correctable   (memory_read_correctable),
    .read_error         (memory_read_error)
);


logic                       test_clock;
logic                       test_reset_n;

logic   [7:0]               network_id;
logic   [15:0]              network_address;
logic   [15:0]              network_data;
logic                       test_stop;


assign cpu_clock            = test_clock;
assign cpu_reset_n          = test_reset_n;
assign cpu_memory_read_data = (cpu_memory_address < 256) ? memory_read_data :
                              (cpu_memory_address == program_code_package::ADDRESS_NETWORK_ID)      ? network_id :
                              (cpu_memory_address == program_code_package::ADDRESS_NETWORK_ADDRESS) ? network_address :
                              (cpu_memory_address == program_code_package::ADDRESS_NETWORK_DATA)    ? network_data :
                              0;

assign memory_write_clock   = test_clock;
assign memory_write_reset_n = test_reset_n;
assign memory_write_enable  = cpu_memory_write_data_enable && (memory_write_address > 255);
assign memory_write_address = cpu_memory_address;
assign memory_write_data    = cpu_memory_write_data;
assign memory_read_clock    = test_clock;
assign memory_read_reset_n  = test_reset_n;
assign memory_read_enable   = !cpu_memory_write_data_enable;
assign memory_read_address  = cpu_memory_address;


always_ff @(posedge test_clock or negedge test_reset_n) begin
    if (!test_reset_n) begin
        cpu_program_code    <= program_code_package::PROGRAM_CODE[program_code_package::MAIN_ADDRESS];
        network_id          <= 0;
        network_address     <= 0;
        network_data        <= 0;
        test_stop           <= 0;
    end
    else begin
        cpu_program_code    <= program_code_package::PROGRAM_CODE[cpu_program_address];

        if (cpu_memory_write_data_enable) begin
            case (cpu_memory_address)
                program_code_package::ADDRESS_NETWORK_ID:       network_id      <= cpu_memory_write_data;
                program_code_package::ADDRESS_NETWORK_ADDRESS:  network_address <= cpu_memory_write_data;
                program_code_package::ADDRESS_NETWORK_DATA:     network_data    <= cpu_memory_write_data;
                program_code_package::ADDRESS_STOP:             test_stop       <= cpu_memory_write_data;
            endcase
        end
    end
end


initial begin
    test_clock = 0;

    forever begin
        #(CLOCK_PERIOD / 2.0) test_clock = !test_clock;
    end
end


initial begin
    reset();

    @(posedge test_clock);
    #(1ns);
    cpu_enable = 1;

    wait (test_stop);
    repeat (10) @(posedge test_clock);
    $stop();
end


task automatic reset();
    test_reset_n    = 0;
    cpu_enable      = 0;
    cpu_interrupt   = 0;

    #(200ns);
    @(posedge test_clock);
    #(1);
    test_reset_n = 1;
endtask


endmodule

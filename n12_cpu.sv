module n12_cpu #(
    parameter           DATA_WIDTH                  = 16,
    parameter           DATA_DEPTH                  = 4,
    parameter           RETURN_DEPTH                = 4,
    parameter   [11:0]  MAIN_ADDRESS                = 0,
    parameter   [11:0]  ISR_ADDRESS                 = 0,
    parameter           ENABLE_INTERRUPT            = 0,
    parameter           ENABLE_ADD                  = 0,
    parameter           ENABLE_LESS_THAN_SIGNED     = 0,
    parameter           ENABLE_LESS_THAN_UNSIGNED   = 0,
    parameter           ENABLE_RIGHT_SHIFT          = 0,
    parameter           ENABLE_MULTIPLY             = 0
) (
    input                               clock,
    input                               reset_n,
    input                               interrupt,
    input                               enable,
    input           [11:0]              program_code,
    input           [DATA_WIDTH-1:0]    memory_read_data,

    output  logic   [8:0]               program_address,
    output  logic   [DATA_WIDTH-1:0]    memory_address,
    output  logic   [DATA_WIDTH-1:0]    memory_write_data,
    output  logic                       memory_write_data_enable
);


typedef enum logic [1:0] {
    OPCODE_LITERAL  = 2'b00,
    OPCODE_CALL     = 2'b01,
    OPCODE_JUMP     = 2'b10,
    OPCODE_ALU      = 2'b11
} opcode_enum;

typedef enum logic [3:0] {
    ALU_TOP                 = 0,
    ALU_NEXT                = 1,
    ALU_RETURN              = 2,
    ALU_MEMORY_READ         = 3,
    ALU_ADD                 = 4,
    ALU_INCREMENT           = 5,
    ALU_DECREMENT           = 6,
    ALU_AND                 = 7,
    ALU_OR                  = 8,
    ALU_XOR                 = 9,
    ALU_NEGATE              = 10,
    ALU_EQUALS              = 11,
    ALU_LESS_THAN_SIGNED    = 12,
    ALU_LESS_THAN_UNSIGNED  = 13,
    ALU_RIGHT_SHIFT         = 14,
    ALU_MULTIPLY            = 15
} alu_enum;


wire                        data_stack_clock;
wire                        data_stack_reset_n;
logic                       data_stack_push_enable;
logic   [DATA_WIDTH-1:0]    data_stack_push_data;
logic                       data_stack_pop_enable;

wire    [DATA_WIDTH-1:0]    data_stack_pop_data;

stack #(
    .WIDTH  (DATA_WIDTH),
    .DEPTH  (DATA_DEPTH-1)
) data_stack(
    .clock          (data_stack_clock),
    .reset_n        (data_stack_reset_n),
    .push_enable    (data_stack_push_enable),
    .push_data      (data_stack_push_data),
    .pop_enable     (data_stack_pop_enable),

    .pop_data       (data_stack_pop_data)
);


wire            return_stack_clock;
wire            return_stack_reset_n;
logic           return_stack_push_enable;
logic   [11:0]  return_stack_push_data;
logic           return_stack_pop_enable;

wire    [11:0]  return_stack_pop_data;

stack #(
    .WIDTH      (12),
    .DEPTH      (RETURN_DEPTH),
    .FILL_VALUE (MAIN_ADDRESS)
) return_stack(
    .clock          (return_stack_clock),
    .reset_n        (return_stack_reset_n),
    .push_enable    (return_stack_push_enable),
    .push_data      (return_stack_push_data),
    .pop_enable     (return_stack_pop_enable),

    .pop_data       (return_stack_pop_data)
);


logic   [1:0]               interrupt_detect;
logic   [1:0]               _interrupt_detect;
logic   [11:0]              program_counter;
logic   [11:0]              _program_counter;
logic   [DATA_WIDTH-1:0]    data_stack_top;
logic   [DATA_WIDTH-1:0]    _data_stack_top;


assign data_stack_clock     = clock;
assign data_stack_reset_n   = reset_n;

assign return_stack_clock   = clock;
assign return_stack_reset_n = reset_n;


always_comb begin
    program_address             = program_counter;
    memory_address              = data_stack_top;
    memory_write_data           = data_stack_pop_data;
    memory_write_data_enable    = 0;

    data_stack_push_enable      = 0;
    data_stack_push_data        = data_stack_top;
    data_stack_pop_enable       = 0;
    return_stack_push_enable    = 0;
    return_stack_push_data      = program_counter + 1;
    return_stack_pop_enable     = 0;

    _interrupt_detect           = interrupt_detect;
    _program_counter            = program_counter;
    _data_stack_top             = data_stack_top;

    if (enable) begin
        program_address     = program_counter + 1;
        _interrupt_detect   = {interrupt_detect, interrupt};
        _program_counter    = program_counter + 1;

        if (ENABLE_INTERRUPT && interrupt_detect == 2'b01) begin
            program_address             = ISR_ADDRESS;
            return_stack_push_enable    = 1;
            return_stack_push_data      = program_counter;
            _program_counter            = ISR_ADDRESS;
        end
        else begin
            case (program_code[11:10]) inside
                OPCODE_LITERAL: begin
                    if (program_code[9]) begin
                        data_stack_push_enable = 1;
                    end

                    if (program_code[8]) begin
                        _data_stack_top = {data_stack_top, program_code[7:0]};
                    end
                    else begin
                        _data_stack_top = program_code[7:0];
                    end
                end
                OPCODE_CALL: begin
                    return_stack_push_enable    = 1;
                    return_stack_push_data      = program_counter + 1;

                    // Vectored call
                    // if (program_code[9]) begin
                    //     program_address     = data_stack_top;
                    //     _program_counter    = data_stack_top;
                    // end
                    // else begin
                        program_address     = program_code[8:0];
                        _program_counter    = program_code[8:0];
                    // end
                end
                OPCODE_JUMP: begin
                    // Pop data for a conditional jump
                    if (program_code[9]) begin
                        data_stack_pop_enable   = 1;
                        _data_stack_top         = data_stack_pop_data;
                    end

                    // Conditional jump/branch if T == 0
                    if ((data_stack_top == 0) || !program_code[9]) begin
                        program_address     = program_code[8:0];
                        _program_counter    = program_code[8:0];
                    end
                end
                OPCODE_ALU: begin
                    data_stack_push_enable      = program_code[9];
                    data_stack_pop_enable       = program_code[8];
                    return_stack_push_enable    = program_code[7];
                    return_stack_pop_enable     = program_code[6];

                    if (program_code[5]) begin
                        program_address     = return_stack_pop_data;
                        _program_counter    = return_stack_pop_data;
                    end

                    memory_write_data_enable    = program_code[4];

                    case (program_code[3:0])
                        ALU_TOP:                _data_stack_top = data_stack_top;
                        ALU_NEXT:               _data_stack_top = data_stack_pop_data;
                        ALU_RETURN:             _data_stack_top = return_stack_pop_data;
                        ALU_MEMORY_READ:        _data_stack_top = memory_read_data;
                        ALU_ADD:                _data_stack_top = ENABLE_ADD ? data_stack_pop_data + data_stack_top : data_stack_top;
                        ALU_INCREMENT:          _data_stack_top = data_stack_top + 1;
                        ALU_DECREMENT:          _data_stack_top = data_stack_top - 1;
                        ALU_AND:                _data_stack_top = data_stack_pop_data & data_stack_top;
                        ALU_OR:                 _data_stack_top = data_stack_pop_data | data_stack_top;
                        ALU_XOR:                _data_stack_top = data_stack_pop_data ^ data_stack_top;
                        ALU_NEGATE:             _data_stack_top = ~data_stack_top;
                        ALU_EQUALS:             _data_stack_top = {DATA_WIDTH{data_stack_pop_data == data_stack_top}};
                        ALU_LESS_THAN_SIGNED:   _data_stack_top = ENABLE_LESS_THAN_SIGNED ? {DATA_WIDTH{$signed(data_stack_pop_data) < $signed(data_stack_top)}} : data_stack_top;
                        ALU_LESS_THAN_UNSIGNED: _data_stack_top = ENABLE_LESS_THAN_UNSIGNED ? {DATA_WIDTH{data_stack_pop_data < data_stack_top}} : data_stack_top;
                        ALU_RIGHT_SHIFT:        _data_stack_top = ENABLE_RIGHT_SHIFT ? data_stack_pop_data >> data_stack_top : data_stack_top;
                        ALU_MULTIPLY:           _data_stack_top = ENABLE_MULTIPLY ? data_stack_pop_data * data_stack_top : data_stack_top;
                    endcase
                end
            endcase
        end
    end
end

always_ff @(posedge clock or negedge reset_n) begin
    if (!reset_n) begin
        interrupt_detect    <= 0;
        program_counter     <= MAIN_ADDRESS;
        data_stack_top      <= 0;
    end
    else begin
        interrupt_detect    <= _interrupt_detect;
        program_counter     <= _program_counter;
        data_stack_top      <= _data_stack_top;
    end
end


endmodule

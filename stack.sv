module stack #(
    parameter               WIDTH       = 16,
    parameter               DEPTH       = 8,
    parameter   [WIDTH-1:0] FILL_VALUE  = 0
) (
    input               clock,
    input               reset_n,
    input               push_enable,
    input   [WIDTH-1:0] push_data,
    input               pop_enable,

    output  [WIDTH-1:0] pop_data
);


logic   [DEPTH-1:0][WIDTH-1:0] memory /* synthesis syn_ramstyle = "registers" */;


assign pop_data = memory[0];


always_ff @(posedge clock or negedge reset_n) begin
    if (!reset_n) begin
        memory <= {DEPTH{FILL_VALUE}};
    end
    else begin
        if (push_enable) begin
            memory[0] <= push_data;

            if (!pop_enable) begin
                for (int i = 0; i < DEPTH - 1; i++) begin
                    memory[i+1] <= memory[i];
                end
            end
        end
        else if (pop_enable) begin
            for (int i = 0; i < DEPTH - 1; i++) begin
                memory[i] <= memory[i+1];
            end
        end
    end
end


endmodule

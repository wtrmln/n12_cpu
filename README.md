# What have we here?

N12 is a 12-bit stack CPU targeted for FPGA implementations and designed to run a small subset of Forth, based on the
concepts in James Bowman's [J1 CPU](https://github.com/jamesbowman/j1). N12 is more of a microcontroller, with more
limited capabilities. Call and jump formats limit the code size to 512 instructions. Program code and any attached
memory are accessed through separate ports.

Rather than cross compiling from Forth, the compiler is written in Python. It outputs program code as a packed array in
Verilog, which can be attached to the CPU by selecting an instruction index with the CPU's `program_address` output and
registering the selected value before passing it into the `program_code` input. Alternatively, a block RAM with a
one-cycle read delay could be used. The intent is to support easily modifying code held in RAM during development and
to allow baking a finished program directly into the bitfile if desired, making the final product more hardware than
software. The Verilog-rendered program code array also includes comments providing traceability to the source for each
line.


# Features

- Architecture
    - 12-bit instructions
    - Parameterized data and return stack depths
    - Parameterized data stack width
    - Parameterized synthesis for some ALU features
    - Memory map interface
    - Edge-triggered interrupt input

- Compiler
    - Integer literals
    - Constants
    - Functions
    - Inline functions
    - If/else
    - Indefinite loops
    - Automatic configuration detection for parameterized ALU features


# Supported words

## Defining words

| Word          | Behavior          | Description |
| ---           | ---               | ---         |
| `:`           | ( -- )            | Defines a new function, e.g.: `: function_name ... ;`. |
| `function`    | ( -- )            | Synonym for `:`. |
| `interrupt`   | ( -- )            | Used with `:` to specify the interrupt service routine, e.g. `: interrupt function_name ... ;`. |
| `inline`      | ( -- )            | Used with `:` to specify a function inserted in text form during compilation, e.g. `: inline function_name ... ;`. |
| `constant`    | ( value -- )      | Defines a new constant, e.g.: `23 constant constant_name`. |


## Program control

| Word                          | Behavior      | Description |
| ---                           | ---           | ---         |
| `if`                          | ( flag -- )   |             |
| `if xxx then zzz`             | ( flag -- )   |             |
| `if xxx else yyy then zzz`    | ( flag -- )   | If flag is true (non-zero) executes `xxx`; otherwise executes `yyy`; continues execution with `zzz`. The phrase `else yyy` is optional. |
| `exit`                        | ( -- )        | Return flow of execution to the address on the return stack. |
| `begin`                       | ( -- )        | Marks the start of an indefinite loop. Usage: `begin ... flag until`, `begin ... flag while ... repeat`, `begin ... again` |
| `until`                       | ( flag -- )   | If flag is false, go back to `begin`. If flag is true, terminate the loop. |
| `while`                       | ( flag -- )   | If flag is true, continue. If flag is false, terminate the loop (after `repeat`). |
| `repeat`                      | ( -- )        | Resolves forward branch from `while`; goes back to `begin`. |
| `again`                       | ( -- )        | Go back to `begin` (infinite loop). |


## Stack manipulation

| Word      | Behavior                      | Description |
| ---       | ---                           | ---         |
| `swap`    | ( n1 n2 -- n2 n1 )            | Reverses the top two stack items. |
| `dup`     | ( n -- n n )                  | Duplicates the top stack item. |
| `over`    | ( n1 n2 -- n1 n2 n1 )         | Copies second item to top. |
| `rot`     | ( n1 n2 n3 -- n2 n3 n1 )      | Rotates third item to top. |
| `drop`    | ( n -- )                      | Discards the top stack item. |

## Memory
| Word      | Behavior      | Description |
| ---       | ---           | ---         |
| `@`       | ( n1 -- n2 )  | Reads from address n1 in memory and pushes value to stack. |
| `!`       | ( n1 n2 -- )  | Writes n1 to address n2 in memory. |


## Relational operations

| Word  | Behavior          | Description |
| ---   | ---               | ---         |
| `=`   | ( n1 n2 -- flag ) | Returns true if n1 and n2 are equal. |
| `<>`  | ( n1 n2 -- flag ) | Returns true if n1 and n2 are not equal. |
| `<`   | ( n1 n2 -- flag ) | Returns true if n1 is less than n2. |
| `>`   | ( n1 n2 -- flag ) | Returns true if n1 is greater than n2. |
| `0=`  | ( n -- flag )     | Returns true if n is zero. |
| `0<`  | ( n -- flag )     | Returns true if n is negative. |
| `0>`  | ( n -- flag )     | Returns true if n is positive. |


## Boolean arithmetic

| Word  | Behavior          | Description |
| ---   | ---               | ---         |
| `~`   | ( n1 -- n2 )      | Returns the bitwise inverse. |
| `&`   | ( n1 n2 -- n3 )   | Returns the bitwise AND. |
| `|`   | ( n1 n2 -- n3 )   | Returns the bitwise OR. |
| `^`   | ( n1 n2 -- n3 )   | Returns the bitwise XOR. |
| `and` | ( n1 n2 -- n3 )   | Returns the logical AND. |
| `or`  | ( n1 n2 -- n3 )   | Returns the logical OR. |

## Math

| Word      | Behavior              | Description |
| ---       | ---                   | ---         |
| `+`       | ( n1 n2 -- sum )      | Adds n1 and n2. |
| `-`       | ( n1 n2 -- sum )      | Subtracts n2 from n1. |
| `*`       | ( n1 n2 -- product)   | Multiplies n1 by n2. |
| `1+`      | ( n1 -- n2 )          | Adds one. |
| `1-`      | ( n1 -- n2 )          | Subtracts one. |
| `abs`     | ( n1 -- n2 )          | Returns the absolute value. |
| `negate`  | ( n1 -- n2 )          | Changes the sign. |
| `min`     | ( n1 n2 -- n3 )       | Returns the minimum. |
| `max`     | ( n1 n2 -- n3 )       | Returns the maximum. |
| `>r`      | ( n -- )              | Takes a value off the parameter stack and pushes it onto the return stack. |
| `r>`      | ( -- n )              | Takes a value off the return stack and pushes it onto the parameter stack. |
| `r@`      | ( -- n )              | Copies the top item from return stack and pushes it onto the parameter stack. |


# Base wordset implementation

See [dictionaries](modules/n12_cpu/dictionaries/).

\ Words relevant to math-y applications
: 0<        0x8000 & ;                      ( n -- flag )   \ Returns true if n is negative.
: 0>        0x8000 < ;                      ( n -- flag )   \ Returns true if n is positive.
: abs       dup 0< if negate then ;         ( n1 -- n2 )    \ Returns the absolute value.
: min       2dup< if drop else nip then ;   ( n1 n2 -- n3 ) \ Returns the minimum.
: max       2dup< if nip else drop then ;   ( n1 n2 -- n3 ) \ Returns the maximum.

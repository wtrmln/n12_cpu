\ Base words that can be expressed in machine code
assemble    noop        0b11_00_00_0_0_0000     ( -- )                  \ Do nothing.
assemble    swap        0b11_11_00_0_0_0001     ( n1 n2 -- n2 n1 )      \ Reverses the top two stack items.
assemble    dup         0b11_10_00_0_0_0000     ( n -- n n )            \ Duplicates the top stack item.
assemble    over        0b11_10_00_0_0_0001     ( n1 n2 -- n1 n2 n1 )   \ Copies second item to top.
assemble    drop        0b11_01_00_0_0_0001     ( n -- )                \ Discards the top stack item.
assemble    nip         0b11_01_00_0_0_0000     ( n1 n2 -- n2 )         \ Discards the item second from the top of the stack.
assemble    =           0b11_01_00_0_0_1011     ( n1 n2 -- flag )       \ Returns true if n1 and n2 are equal.
assemble    <           0b11_01_00_0_0_1100     ( n1 n2 -- flag )       \ Returns true if n1 is less than n2, signed comparison.
assemble    u<          0b11_01_00_0_0_1101     ( n1 n2 -- flag )       \ Returns true if n1 is less than n2, unsigned comparison.
assemble    and         0b11_01_00_0_0_0111     ( n1 n2 -- n3 )         \ Returns the logical AND.
assemble    or          0b11_01_00_0_0_1000     ( n1 n2 -- n3 )         \ Returns the logical OR.
assemble    +           0b11_01_00_0_0_0100     ( n1 n2 -- sum )        \ Adds n1 and n2.
assemble    *           0b11_01_00_0_0_1111     ( n1 n2 -- product )    \ Multiplies n1 by n2.
assemble    1+          0b11_00_00_0_0_0101     ( n1 -- n2 )            \ Adds one.
assemble    1-          0b11_00_00_0_0_0110     ( n1 -- n2 )            \ Subtracts one.
assemble    >r          0b11_01_10_0_0_0001     ( n -- )                \ Takes a value off the data stack and pushes it onto the return stack.
assemble    r>          0b11_10_01_0_0_0010     ( -- n )                \ Takes a value off the return stack and pushes it onto the data stack.
assemble    r@          0b11_10_00_0_0_0010     ( -- n )                \ Copies the top item from return stack and pushes it onto the data stack.
assemble    &           0b11_01_00_0_0_0111     ( n1 n2 -- n3 )         \ Returns the bitwise AND.
assemble    |           0b11_01_00_0_0_1000     ( n1 n2 -- n3 )         \ Returns the bitwise OR.
assemble    ^           0b11_01_00_0_0_1001     ( n1 n2 -- n3 )         \ Returns the bitwise XOR.
assemble    xor         0b11_01_00_0_0_1001     ( n1 n2 -- n3 )         \ Returns the bitwise XOR.
assemble    ~           0b11_00_00_0_0_1010     ( n1 -- n2 )            \ Returns the bitwise inverse.
assemble    rshift      0b11_01_00_0_0_1110     ( n1 n2 -- n1_>>_n2 )   \ Shifts n1 to the right by n2 bits.
assemble    exit        0b11_00_01_1_0_0000     ( -- )                  \ Return flow of execution to the address on the return stack.

\ Machine-specific assembly used to build up some words
assemble    N->[T]      0b11_01_00_0_1_0001     ( n1 n2 -- )            \ Writes value n1 to memory address n2.
assemble    [T]         0b11_00_00_0_0_0011     ( n1 -- [n1] )          \ Pushes value from memory address n1 onto the data stack.

\ Elided words expressed in machine-specific assembly
assemble    2dupand     0b11_10_00_0_0_0111     ( n1 n2 -- n1 n2 n1_and_n2 )    \ Pushes n1 and n2 to the data stack on top of its operands.
assemble    2dup<       0b11_10_00_0_0_1100     ( n1 n2 -- n1 n2 n1_<_n2 )      \ Pushes n1 < n2 to the data stack on top of its operands.
assemble    2dup=       0b11_10_00_0_0_1011     ( n1 n2 -- n1 n2 n1_=_n2 )      \ Pushes n1 = n2 to the data stack on top of its operands.
assemble    2dupor      0b11_10_00_0_0_1000     ( n1 n2 -- n1 n2 n1_or_n2 )     \ Pushes n1 or n2 to the data stack on top of its operands.
assemble    2duprshift  0b11_10_00_0_0_1110     ( n1 n2 -- n1 n2 n1_>>_n2 )     \ Pushes n1 rshift n2 to the data stack on top of its operands.
assemble    2dup+       0b11_10_00_0_0_0100     ( n1 n2 -- n1 n2 n1_+_n2 )      \ Pushes n1 + n2 to the data stack on top of its operands.
assemble    2dupu<      0b11_10_00_0_0_1101     ( n1 n2 -- n1 n2 n1_u<_n2 )     \ Pushes n1 u< n2 to the data stack on top of its operands.
assemble    2dupxor     0b11_10_00_0_0_1001     ( n1 n2 -- n1 n2 n1_^_n2 )      \ Pushes n1 xor n2 to the data stack on top of its operands.
assemble    2dup^       0b11_10_00_0_0_1001     ( n1 n2 -- n1 n2 n1_^_n2 )      \ Pushes n1 ^ n2 to the data stack on top of its operands.
assemble    dup>r       0b11_00_10_0_0_0000     ( n1 -- n1 )                    \ Copies n1 to the return data stack without popping it.
assemble    overand     0b11_00_00_0_0_0111     ( n1 n2 -- n1 n1_and_n2 )       \ Pushes n1 and n2 to the data stack on top of n1.
assemble    over>       0b11_00_00_0_0_1100     ( n1 n2 -- n1 n1_<_n2 )         \ Pushes n1 < n2 to the data stack on top of n1.
assemble    over=       0b11_00_00_0_0_1011     ( n1 n2 -- n1 n1_=_n2 )         \ Pushes n1 = n2 to the data stack on top of n1.
assemble    overor      0b11_00_00_0_0_1000     ( n1 n2 -- n1 n1_or_n2 )        \ Pushes n1 or n2 to the data stack on top of n1.
assemble    over+       0b11_00_00_0_0_0100     ( n1 n2 -- n1 n1_+_n2 )         \ Pushes n1 + n2 to the data stack on top of n1.
assemble    overu>      0b11_00_00_0_0_1101     ( n1 n2 -- n1 n1_u<_n2 )        \ Pushes n1 u< n2 to the data stack on top of n1.
assemble    overxor     0b11_00_00_0_0_1001     ( n1 n2 -- n1 n1_xor_n2 )       \ Pushes n1 xor n2 to the data stack on top of n1.
assemble    rdrop       0b11_00_01_0_0_0000     ( -- )                          \ Drops the top of the return stack with no other effects.
assemble    tuck!       0b11_01_00_0_1_0000     ( n1 n2 -- n1 )                 \ Writes n1 to memory address n2 without popping n1.

\ Inline words
: inline    <>          = ~ ;           ( n1 n2 -- flag )   \ Returns true if n1 and n2 are not equal.
: inline    >           < ~ ;           ( n1 n2 -- flag )   \ Returns true if n1 is greater than n2.
: inline    0=          0 = ;           ( n -- flag )       \ Returns true if n is zero.
: inline    negate      ~ 1+ ;          ( n1 -- n2 )        \ Changes the sign.
: inline    -           negate + ;      ( n1 n2 -- sum )    \ Subtracts n2 from n1.
: inline    !           N->[T] drop ;   ( n1 n2 -- )        \ Writes n1 to address n2 in memory.
: inline    @           noop [T] ;      ( n1 -- [n1] )      \ Reads from address n1 in memory and pushes value to stack.
: inline    dup@        dup [T] ;       ( n1 -- n1 [n1] )   \ Reads from address n1 in memory and pushes value to stack without popping n1.

\ Basic words too long to inline
: rot       >r swap r> swap ;   ( n1 n2 n3 -- n2 n3 n1 )    \ Rotates third item to top.

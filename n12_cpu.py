import re, os


OPCODE_LITERAL_PUSH     = 0b00_10_00000000
OPCODE_LITERAL_SHIFT    = 0b00_01_00000000

OPCODE_CALL             = 0b01_1_000000000

OPCODE_JUMP             = 0b10_0_000000000
OPCODE_JUMP_IF_ZERO     = 0b10_1_000000000

OPCODE_ALU              = 0b11_00_00_0_0_0000

OPCODE_ALU_PUSH_DATA    = 0b00_10_00_0_0_0000
OPCODE_ALU_POP_DATA     = 0b00_01_00_0_0_0000
OPCODE_ALU_PUSH_RETURN  = 0b00_00_10_0_0_0000
OPCODE_ALU_POP_RETURN   = 0b00_00_01_0_0_0000
OPCODE_ALU_EXIT         = 0b00_00_00_1_0_0000
OPCODE_ALU_MEMORY_WRITE = 0b00_00_00_0_1_0000

ALU_MASK                = 0B00_00_00_0_0_1111

ALU_TOP                 = 0
ALU_NEXT                = 1
ALU_RETURN              = 2
ALU_MEMORY_READ         = 3
ALU_ADD                 = 4
ALU_INCREMENT           = 5
ALU_DECREMENT           = 6
ALU_AND                 = 7
ALU_OR                  = 8
ALU_XOR                 = 9
ALU_NEGATE              = 10
ALU_EQUALS              = 11
ALU_LESS_THAN_SIGNED    = 12
ALU_LESS_THAN_UNSIGNED  = 13
ALU_RIGHT_SHIFT         = 14
ALU_MULTIPLY            = 15

NUMERIC_REGEX           = re.compile(r"\d+?'?(?P<base>[bdhxBDHX])(?P<num>[0-9A-Fa-f_]+)")


def is_integer(number):
    if isinstance(number, (bool, float, int)):
        return True
    elif isinstance(number, str):
        if NUMERIC_REGEX.match(number):
            return True
        else:
            try:
                if int(float(number)) != None:
                    return True
            except ValueError:
                return False

    return False


def to_integer(number, pattern=NUMERIC_REGEX):
    if isinstance(number, (bool, float)):
        return int(number)
    elif isinstance(number, str):
        match = pattern.match(number)
        if match:
            base, num = match.group(1).lower(), match.group(2)
            if base == "b":
                base = 2
            elif base == "d":
                base = 10
            elif base == "h" or base == "x":
                base = 16
            return int(num, base=base)
        else:
            return int(float(number))
    elif isinstance(number, int):
        return number
    else:
        raise ValueError("Value \"{}\" is not a valid integer.".format(number))


# Represents a string of source text.
class source_token:
    def __init__(self, source, text, line):
        self.source = source
        self.text   = text
        self.line   = line


# Represents a single machine instruction and the source token that generated it.
class machine_instruction():
    def __init__(self, token, value):
        self.token  = token
        self.value  = value


# Represents a pointer to code or contains direct code.
class code_reference:
    def __init__(self, token=None, instructions=[], address=None):
        self.token          = token
        self.instructions   = instructions
        self.address        = address


def make_noop(token):
    return [machine_instruction(token=token, value=OPCODE_ALU | ALU_TOP)]


def make_exit(token):
    return [machine_instruction(token=token, value= OPCODE_ALU | OPCODE_ALU_EXIT | OPCODE_ALU_POP_RETURN)]


# Convert a numeric value to machine opcode(s) for a literal.
def make_literal(token, value):
    value_int = to_integer(value)
    if value_int > 65535:
        raise ValueError("Constant \"{}\" exceeds maximum of 65535.".format(value_int))

    if value_int < 256:
        return [machine_instruction(token=token, value=OPCODE_LITERAL_PUSH | value_int)]
    else:
        return [
            machine_instruction(token=token, value=OPCODE_LITERAL_PUSH | ((value_int >> 8) & 255)),
            machine_instruction(token=token, value=OPCODE_LITERAL_SHIFT | (value_int & 255))
        ]


# Convert a numeric value to a raw binary machine opcode.
def make_assembly(token, value):
    value_int = to_integer(value)
    if value_int >= 2 ** 12:
        raise ValueError("Assembly \"{}\" is not a valid instruction.".format(value_int))

    return [machine_instruction(token=token, value=value_int)]


# Convert a numeric value into a function call opcode.
def make_call(token, target):
    target_int = to_integer(target)
    if target_int >= 2 ** 9:
        raise ValueError("Call target \"{}\" is not a valid location.".format(target_int))

    return [machine_instruction(token=token, value=OPCODE_CALL | target_int)]


# Convert a numeric value into a jump opcode.
def make_jump(token, target, conditional):
    target_int = to_integer(target)
    if target_int >= 2 ** 9:
        raise ValueError("Jump target \"{}\" is not a valid location.".format(target_int))

    if conditional:
        return [machine_instruction(token=token, value=OPCODE_JUMP_IF_ZERO | target_int)]
    else:
        return [machine_instruction(token=token, value=OPCODE_JUMP | target_int)]


class program_binary:
    def __init__(self, program_code, constants, main_address, isr_address):
        self.program_code               = program_code
        self.constants                  = constants
        self.main_address               = main_address
        self.isr_address                = isr_address
        self.enable_interrupt           = isr_address is not None
        self.enable_add                 = False
        self.enable_less_than_signed    = False
        self.enable_less_than_unsigned  = False
        self.enable_right_shift         = False
        self.enable_multiply            = False

        # Automatically parameterize opcode-specific logic synthesis
        for instruction in program_code:
            if (instruction.value & OPCODE_ALU) == OPCODE_ALU:
                if (instruction.value & ALU_MASK) == ALU_ADD:
                    self.enable_add = True
                elif (instruction.value & ALU_MASK) == ALU_LESS_THAN_SIGNED:
                    self.enable_less_than_signed = True
                elif (instruction.value & ALU_MASK) == ALU_LESS_THAN_UNSIGNED:
                    self.enable_less_than_unsigned = True
                elif (instruction.value & ALU_MASK) == ALU_RIGHT_SHIFT:
                    self.enable_right_shift = True
                elif (instruction.value & ALU_MASK) == ALU_MULTIPLY:
                    self.enable_multiply = True

    def render_verilog(self):
        # Convert program code to a Verilog array
        verilog_hdl = ""

        verilog_hdl += "parameter logic ENABLE_INTERRUPT            = 1'b{:1b};\n".format(self.enable_interrupt)
        verilog_hdl += "parameter logic ENABLE_ADD                  = 1'b{:1b};\n".format(self.enable_interrupt)
        verilog_hdl += "parameter logic ENABLE_LESS_THAN_SIGNED     = 1'b{:1b};\n".format(self.enable_interrupt)
        verilog_hdl += "parameter logic ENABLE_LESS_THAN_UNSIGNED   = 1'b{:1b};\n".format(self.enable_interrupt)
        verilog_hdl += "parameter logic ENABLE_RIGHT_SHIFT          = 1'b{:1b};\n".format(self.enable_interrupt)
        verilog_hdl += "parameter logic ENABLE_MULTIPLY             = 1'b{:1b};\n".format(self.enable_interrupt)

        verilog_hdl += "\n\n"
        verilog_hdl += "parameter logic [11:0] MAIN_ADDRESS = 12'd{};\n".format(self.main_address)
        verilog_hdl += "parameter logic [11:0] ISR_ADDRESS  = 12'd{};\n".format(self.isr_address if self.isr_address is not None else self.main_address)

        verilog_hdl += "\n\n"
        if self.constants:
            verilog_hdl += "// Constants\n"
            width = max([len(name) for name in self.constants])
            for name, value in self.constants.items():
                verilog_hdl += "parameter {name:{width}} = {value};\n".format(
                    name    = name.upper(),
                    width   = width,
                    value   = value)

        verilog_hdl += "\n\n"
        verilog_hdl += "parameter logic [0:{}][11:0] PROGRAM_CODE = {{".format(len(self.program_code) - 1)
        for index, instruction in enumerate(self.program_code):
            comma = "," if index < len(self.program_code) - 1 else " "
            verilog_hdl += "\n    /* {:3d} */   12'b{:012b}".format(index, instruction.value)
            verilog_hdl += "{}    // {}:{:3d}: {}".format(comma, instruction.token.source, instruction.token.line, instruction.token.text)
        verilog_hdl += "\n};\n"

        return verilog_hdl

    def render_binary(self):
        return b''.join(instruction.value.to_bytes(2, byteorder="big") for instruction in self.program_code)


# Holds the compiled state of the program. Call `compile` to add sources to the program.
class compiler:
    def __init__(self):
        self.clear();

    # May be used either prior to or during compilation to add named constants to the dictionary.
    def add_constant(self, name, value, token=None):
        if name in self.target_dictionary:
            raise KeyError("\"{}\" already exists in the dictionary".format(name))

        self.constants[name]            = value
        self.target_dictionary[name]    = code_reference(
            token           = token,
            instructions    = make_literal(
                token   = token or source_token(
                    source  = "external",
                    line    = 0,
                    text    = "{}: {}".format(name, value)),
                value   = value))

    def instruction_pointer(self):
        return len(self.program_code)

    def clear(self):
        self.constants          = {};
        self.target_dictionary  = {};
        self.program_code       = [];
        self.isr_address        = None;

        # Add base words to the source.
        base_path       = os.path.join(os.path.dirname(__file__), "base.fth")
        base_rel_path   = os.path.relpath(base_path)
        self.compile_file(base_rel_path)

    def dump_target_dictionary(self):
        for name, reference in self.target_dictionary.items():
            print("\"{}\":".format(name))
            print("    line {}: \"{}\"".format(reference.token.line, reference.token.text))
            print("    address: {}, code: [{}]".format(
                "None" if reference.address is None else "{:03d}".format(reference.address),
                ",".join(["0b{:012b}".format(instruction.value) for instruction in reference.instructions])))

    def add_instructions(self, instructions):
        self.program_code.extend(instructions)

    def compile_file(self, path):
        with open(path, "r") as source_file:
            return self.compile_string(source_name=os.path.relpath(path), source_string=source_file.read())

    def compile_string(self, source_name, source_string):
        reference_stack = []
        tokens          = []
        token_index     = 0

        # Convert source lines to tokens
        for i, line in enumerate(source_string.split("\n")):
            for string in line.split():
                tokens.append(source_token(source=source_name, text=string, line=i+1))

        # For token, line in stream:
        while tokens:
            token = tokens.pop(0)

            if token.text == "(":
                # Drop tokens until a close comment is found
                while tokens and (tokens.pop(0).text != ")"):
                    pass

            elif token.text == "\\":
                # Drop tokens until the end of the current line
                while tokens and (tokens[0].line == token.line):
                    tokens.pop(0)

            elif token.text == "constant":
                # Consume next token as name and top of stack as value
                value_token = reference_stack.pop().token
                name_token  = tokens.pop(0)
                new_token   = source_token(
                    source  = source_name,
                    text    = "{} constant {}".format(value_token.text, name_token.text),
                    line    = value_token.line)
                # Add to target dictionary with code pointer = literal instruction
                self.add_constant(name=name_token.text, value=value_token.text, token=new_token)

            elif token.text == "assemble" or token.text == "::":
                # Consume next token as a name, and following as value
                name_token  = tokens.pop(0)
                value_token = tokens.pop(0)
                new_token   = source_token(
                    source  = source_name,
                    text    = "assemble {} {}".format(name_token.text, value_token.text),
                    line    = token.line)
                # Add to target dictionary with code pointer set to binary value
                if name_token.text in self.target_dictionary:
                    raise KeyError("\"{}\" already exists in the dictionary".format(name_token.text))

                self.target_dictionary[name_token.text] = code_reference(
                    token           = new_token,
                    instructions    = make_assembly(token=name_token, value=value_token.text))

            elif token.text == "function" or token.text == ":":
                # The last function to be defined is considered "main", even if it's an interrupt
                self.main_address   = None
                function_length     = 0

                # Consume next token as name
                name_token = tokens.pop(0)

                if name_token.text == "interrupt":
                    # Consume another token as name
                    name_token = tokens.pop(0)
                    # Set interrupt address to current instruction pointer
                    self.isr_address = self.instruction_pointer()
                    new_token = source_token(
                        source  = source_name,
                        text    = "{} interrupt {}".format(token.text, name_token.text),
                        line    = token.line)
                elif name_token.text == "inline":
                    # Create a reference to a concatenation of tokens rather than compiling those tokens into a new word
                    # definition.
                    inline_list = []
                    name_token  = tokens.pop(0)

                    while tokens:
                        inline_token = tokens.pop(0)

                        if inline_token.text == ";":
                            if name_token.text in self.target_dictionary:
                                raise KeyError("\"{}\" already exists in the dictionary".format(name_token.text))

                            # Add to target dictionary with code set to tokens of function body
                            self.target_dictionary[name_token.text] = code_reference(
                                token           = None,
                                instructions    = inline_list)
                            break
                        else:
                            inline_list.append(inline_token)
                    continue
                else:
                    new_token = source_token(
                        source  = source_name,
                        text    = "{} {}".format(token.text, name_token.text),
                        line    = token.line)

                # Add function to target dictionary with call instruction targeting the current instruction pointer
                if name_token.text in self.target_dictionary:
                    raise KeyError("\"{}\" already exists in the dictionary".format(name_token.text))

                function_reference = code_reference(
                    token           = new_token,
                    instructions    = make_call(token=new_token, target=self.instruction_pointer()),
                    address         = self.instruction_pointer())
                self.target_dictionary[name_token.text] = function_reference

                while tokens:
                    function_token = tokens.pop(0)

                    if function_token.text == "(":
                        # Drop tokens until a close comment is found
                        while tokens and (tokens.pop(0).text != ")"):
                            pass

                    elif function_token.text == "\\":
                        # Drop tokens until the end of the current line
                        while tokens and (tokens[0].line == function_token.line):
                            tokens.pop(0)

                    elif function_token.text == "if":
                        # Push instruction to the stack and compile with empty machine code
                        reference_stack.append(code_reference(
                            token   = function_token,
                            address = self.instruction_pointer()))
                        self.add_instructions(make_noop(function_token))

                    elif function_token.text == "else":
                        # If top of stack's token is "if":
                        if_reference = reference_stack.pop()
                        if if_reference.token.text == "if":
                            # Replace code at popped instruction's address with jump to current instruction pointer
                            self.program_code[if_reference.address] = make_jump(
                                token       = if_reference.token,
                                target      = self.instruction_pointer(),
                                conditional = True)[0] # This is kinda jank.
                        else:
                            raise ValueError("line {}: \"else\" with no matching \"if\".")
                        # Push uncompiled instruction to the stack
                        reference_stack.append(code_reference(
                            token   = function_token,
                            address = self.instruction_pointer()))

                    elif function_token.text == "then":
                        # If top of stack's token is "if":
                        if_reference = reference_stack.pop()
                        if if_reference.token.text == "if":
                            # Set popped instruction's machine code to jump to current instruction pointer
                            self.program_code[if_reference.address] = make_jump(
                                token       = if_reference.token,
                                target      = self.instruction_pointer(),
                                conditional = True)[0] # This is kinda jank.
                        elif if_reference.token.text == "else":
                            self.program_code[if_reference.address].token.text = "else {}".format(
                                self.program_code[if_reference.address].token.text)
                        else:
                            raise ValueError("line {}: \"then\" with no matching \"if\" or \"else\".")

                    elif function_token.text == "begin":
                        # Push instruction to the stack and compile with empty machine code
                        reference_stack.append(code_reference(
                            token   = function_token,
                            address = self.instruction_pointer()))

                    elif function_token.text == "until":
                        # If top of stack's token is "begin":
                        begin_reference = reference_stack.pop()
                        if begin_reference.token.text == "begin":
                            # Compile conditional jump to popped instruction's address
                            self.add_instructions(make_jump(
                                token       = function_token,
                                target      = begin_reference.address,
                                conditional = True))
                            # Tag begin token
                            begin_token         = self.program_code[begin_reference.address].token
                            begin_token.text    = "begin {}".format(begin_token.text)
                        else:
                            raise ValueError("line {}: \"until\" with no matching \"begin\".")

                    elif function_token.text == "while":
                        # If top of stack's token is "begin":
                        if reference_stack[-1].token.text == "begin":
                            # Compile empty machine code and push instruction to the stack
                            reference_stack.append(code_reference(
                                token   = function_token,
                                address = self.instruction_pointer()))
                            self.add_instructions(make_noop(token=function_token))
                        else:
                            raise ValueError("line {}: \"while\" with no matching \"begin\".")

                    elif function_token.text == "repeat":
                        # If top of stack's token is "while":
                        while_reference = reference_stack.pop()
                        if while_reference.token.text == "while":
                            # If top of stack's token is "begin":
                            begin_reference = reference_stack.pop()
                            if begin_reference.token.text == "begin":
                                # Compile unconditional jump to begin's address
                                self.add_instructions(make_jump(
                                    token       = function_token,
                                    target      = begin_reference.address,
                                    conditional = False))
                                # Set while's machine code to jump to current instruction pointer
                                self.program_code[while_reference.address] = make_jump(
                                        token       = while_reference.token,
                                        target      = self.instruction_pointer(),
                                        conditional = False)[0] # This is kinda jank.
                                # Tag begin token
                                begin_token         = self.program_code[begin_reference.address].token
                                begin_token.text    = "begin {}".format(begin_token.text)
                            else:
                                raise ValueError("line {}: \"while\" with no matching \"begin\".")
                        else:
                            raise ValueError("line {}: \"repeat\" with no matching \"while\".")

                    elif function_token.text == "again":
                        # If top of stack's token is "begin":
                        begin_reference = reference_stack.pop()
                        if begin_reference.token.text == "begin":
                            # Compile unconditional jump to begin's address
                            self.add_instructions(make_jump(
                                token       = function_token,
                                target      = begin_reference.address,
                                conditional = False))
                            begin_token = self.program_code[begin_reference.address].token
                            begin_token.text = "begin {}".format(begin_token.text)

                        else:
                            raise ValueError("line {}: \"again\" with no matching \"begin\".")

                    elif function_token.text == ";":
                        # Error if stack not empty
                        if reference_stack:
                            error_token = reference_stack.pop()
                            raise ValueError("line {}: mismatched \"{}\" from function \"{}\".".format(
                                error_token.line,
                                error_token.text,
                                function_token.text))

                        if not function_length:
                            raise ValueError("line {}: empty function \"{}\".".format(
                                function_reference.token.line,
                                function_reference.token.text))

                        # Tag start token with function name
                        start_token = self.program_code[function_reference.address].token
                        start_token.text = ": {} {}".format(name_token.text, start_token.text)

                        # The last defined function will become main, the program's entry point
                        self.main_address = function_reference.address

                        # Compile return, optimizing into previous ALU instruction if possible
                        last_instruction = self.program_code[-1]
                        if ((last_instruction.value & OPCODE_ALU) == OPCODE_ALU) and not (last_instruction.value & OPCODE_ALU_POP_RETURN):
                            last_instruction.value |= OPCODE_ALU_POP_RETURN | OPCODE_ALU_EXIT
                            last_instruction.token.text = "{} ;".format(last_instruction.token.text)
                        else:
                            self.add_instructions(make_exit(token = function_token))

                        break

                    elif function_token.text in self.target_dictionary:
                        # If token is function name and next token is ";":
                        if (function_token.text == name_token.text) and (tokens[0].text == ";"):
                                # Compile unconditional jump to start of function
                                new_token = source_token(
                                    source  = source_name,
                                    text    = "{} (tail call optimized recursion)".format(name_token.text),
                                    line    = function_token.line)
                                self.add_instructions(make_jump(
                                    token       = new_token,
                                    target      = function_reference.address,
                                    conditional = False))
                        elif self.target_dictionary[function_token.text].token is None:
                            # Word is an inline function; prepend its tokens to the input stream with altered token text
                            tagged_inline_tokens = [
                                source_token(
                                    source  = "({}:{}: {}) {}".format(
                                        function_token.source,
                                        function_token.line,
                                        function_token.text,
                                        inline_token.source),
                                    text    = inline_token.text,
                                    line    = inline_token.line)
                                for inline_token in self.target_dictionary[function_token.text].instructions]
                            tokens = tagged_inline_tokens + tokens

                        else:
                            # Compile word's code or code pointer
                            word_instructions = self.target_dictionary[function_token.text].instructions
                            updated_instructions = [
                                machine_instruction(
                                    token   = function_token,
                                    value   = instruction.value)
                                for instruction in word_instructions]
                            self.add_instructions(updated_instructions)

                    elif is_integer(function_token.text):
                        # Compile literal instruction
                        self.add_instructions(make_literal(
                            token   = function_token,
                            value   = function_token.text))

                    else:
                        raise ValueError("line {}: unknown identifier \"{}\".".format(function_token.line, function_token.text))

                    function_length += 1

            elif is_integer(token.text):
                # Push onto the stack
                reference_stack.append(code_reference(token=token, address=self.instruction_pointer()))

            else:
                raise ValueError("line {}: unknown identifier \"{}\".".format(token.line, token.text))

        if self.main_address is None:
            raise ValueError("no main function defined, is the last function missing a ; ?")

        if reference_stack:
            raise ValueError("items left on reference stack: {}".format(reference_stack))

        return program_binary(
            program_code    = self.program_code,
            constants       = self.constants,
            main_address    = self.main_address,
            isr_address     = self.isr_address)
